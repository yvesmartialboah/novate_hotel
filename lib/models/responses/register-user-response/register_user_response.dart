import 'package:json_annotation/json_annotation.dart';

part 'register_user_response.g.dart';

@JsonSerializable(explicitToJson: true)
class RegisterUserResponse {
  String id;
  String phone;
  String firstname;
  String lastname;
  String password;
  String email;

  RegisterUserResponse({
    required this.id,
    required this.phone,
    required this.firstname,
    required this.lastname,
    required this.password,
    required this.email,
  });

  factory RegisterUserResponse.fromJson(Map<String, dynamic> json) =>
      _$RegisterUserResponseFromJson(json);

  Map<String, dynamic> toJson() => _$RegisterUserResponseToJson(this);
}
