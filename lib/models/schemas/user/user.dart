import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  String? id;
  String phone;
  String firstname;
  String lastname;
  String password;
  String email;
  List<String>? roles;

  User(this.id, this.phone, this.firstname, this.lastname, this.password,
      this.email, this.roles);

  User.withValues(
      {this.id,
      required this.phone,
      required this.firstname,
      required this.lastname,
      required this.password,
      required this.email,
      this.roles});

  User clone({id, phone, firstname, lastname, password, email, roles}) {
    return User.withValues(
        id: id ?? this.id,
        phone: phone ?? this.phone,
        firstname: firstname ?? this.firstname,
        lastname: lastname ?? this.lastname,
        password: password ?? this.password,
        email: email ?? this.email,
        roles: roles ?? this.roles);
  }

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
