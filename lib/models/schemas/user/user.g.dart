// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    json['@id'] as String,
    json['firstname'] as String,
    json['lastname'] as String,
    json['phone'] as String,
    json['password'] as String,
    json['email'] as String,
    json['roles'] as List<String>,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      '@id': instance.id,
       'firstname' : instance.firstname,
       'lastname' : instance.lastname,
       'phone' : instance.phone,
       'password' : instance.password,
       'email' : instance.email,
       'roles' : instance.roles, 
    };
