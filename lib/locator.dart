import 'package:get_it/get_it.dart';
import 'package:novate_hotel_app/app_config.dart';
import 'package:novate_hotel_app/services/navigation_service.dart';

final locator = GetIt.instance;

void setupLocator(AppConfig config) {
  locator.registerLazySingleton<NavigationService>(() => NavigationService());
}
