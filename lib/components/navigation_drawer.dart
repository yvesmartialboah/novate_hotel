import 'package:flutter/material.dart';
import 'package:novate_hotel_app/constants/img_urls.dart';
import 'package:novate_hotel_app/screens/dashboard/room_tab/booking_list.dart';
import 'package:novate_hotel_app/screens/dashboard/room_tab/booking_room_start_date.dart';

class NavigationDrawer extends StatelessWidget {
  final padding = const EdgeInsets.symmetric(horizontal: 20);

  const NavigationDrawer({Key? key, required this.userFullName})
      : super(key: key);
  final String userFullName;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Material(
        color: Colors.white,
        child: ListView(
          children: <Widget>[
            Container(
              alignment: Alignment.topCenter,
              margin: const EdgeInsets.only(top: 15),
              child: Column(
                children: <Widget>[
                  Container(
                    width: 130,
                    height: 130,
                    margin: const EdgeInsets.only(bottom: 10),
                    child: const CircleAvatar(
                      backgroundImage: AssetImage(roomImg),
                    ),
                  ),
                  const Text(
                    'Bonjour !',
                    style: TextStyle(fontSize: 17),
                  ),
                  Text(
                    userFullName,
                    style: const TextStyle(
                        fontSize: 25, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Divider(thickness: 1.0, color: Color.fromRGBO(0, 0, 0, 0.12)),
            buildMenuItem(
                text: 'Mes réservations',
                icon: ticketIcon,
                onClicked: () => selectedItem(context, 0)),
            const SizedBox(
              height: 16,
            ),
            buildMenuItem(
                text: 'Contacts',
                icon: phoneIcon,
                onClicked: () => selectedItem(context, 1)),
            const SizedBox(
              height: 250,
            ),
            const Divider(thickness: 1.0, color: Color.fromRGBO(0, 0, 0, 0.12)),
            buildMenuItem(
                text: 'Configurations',
                icon: Icons.settings,
                onClicked: () => selectedItem(context, 1)),
            const Divider(thickness: 1.0, color: Color.fromRGBO(0, 0, 0, 0.12)),
            buildMenuItem(
                text: 'Se déconnecter',
                icon: Icons.logout,
                onClicked: () => selectedItem(context, 1)),
          ],
        ),
      ),
    );
  }

  Widget buildMenuItem({
    required String text,
    required dynamic icon,
    VoidCallback? onClicked,
  }) {
    const color = Colors.black;
    const hoverColor = Colors.black38;

    return ListTile(
      leading: icon.runtimeType == String ? Image.asset(icon) : Icon(icon),
      title: Text(
        text,
        style: const TextStyle(color: color, fontSize: 17),
      ),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }

  void selectedItem(BuildContext context, int i) {
    Navigator.of(context).pop();

    switch (i) {
      case 0:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => const BookingList()));
        break;
    }
  }
}
