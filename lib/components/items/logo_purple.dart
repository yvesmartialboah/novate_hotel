import 'package:flutter/material.dart';
import 'package:novate_hotel_app/constants/colors.dart';
import 'package:novate_hotel_app/constants/img_urls.dart';

Widget LogoPurple() {
  return Row(
    children: <Widget>[
      SizedBox(
        height: 35,
        width: 35,
        child: Image.asset(
          hotelLogo62,
          fit: BoxFit.contain,
        ),
      ),
      const Text(
        'NovHôtel',
        style: TextStyle(
            color: colorPurlpleShade,
            fontSize: 23,
            fontWeight: FontWeight.w400),
      )
    ],
  );
}
