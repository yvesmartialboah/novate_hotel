import 'package:flutter/material.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:novate_hotel_app/constants/img_urls.dart';

class SliderCustomTab extends StatelessWidget {
  const SliderCustomTab({Key? key, required this.slide}) : super(key: key);

  final Slide slide;

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    return Container(
        child: Column(
      children: [
        Container(
          child: Image.asset(
            slide.pathImage ?? roomImg,
            width: screenWidth * 0.7,
            height: screenHeight * 0.3,
            fit: BoxFit.fill,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: screenHeight * 0.04),
          child: Text(
            slide.title ?? '',
            style: slide.styleTitle,
            textAlign: TextAlign.center,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20),
          child: Text(
            slide.description ?? '',
            style: slide.styleDescription,
            textAlign: TextAlign.center,
            maxLines: 4,
          ),
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
        ),
      ],
    ));
  }
}
