import 'package:flutter/material.dart';
import 'package:novate_hotel_app/components/loader.dart';
import 'package:novate_hotel_app/constants/colors.dart';

Widget CustomButton(
    {required BuildContext contextProp,
    required Function onPressedProp,
    required String textProp,
    bool disabledProp = false,
    bool loader = false,
    bool fillGradient = false}) {
  return Container(
      child: IgnorePointer(
          ignoring: disabledProp,
          child: Opacity(
            opacity: disabledProp ? 0.5 : 1,
            child: TextButton(
              onPressed: () {
                onPressedProp();
              },
              child: Container(
                  alignment: Alignment.center,
                  height: 55.0,
                  width: MediaQuery.of(contextProp).size.width - 45,
                  decoration: BoxDecoration(
                      gradient: fillGradient == true
                          ? const LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                              colors: [colorPurple, colorBlue])
                          : null,
                      color: fillGradient == true ? null : colorPurlpleShade,
                      borderRadius: BorderRadius.circular(8)),
                  child: loader
                      ? Loader(size: 15, color: Colors.white)
                      : Text(textProp,
                          style: const TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                          ))),
            ),
          )));
}
