import 'package:flutter/material.dart';
import 'package:novate_hotel_app/app_config.dart';
import 'package:novate_hotel_app/screens/root/root_screen.dart';
import 'package:novate_hotel_app/ioc_locator.dart';
import 'package:novate_hotel_app/locator.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  var configuredApp = const AppConfig(
    appName: 'Novate Hotel',
    flavorName: 'production',
    apiBaseUrl: 'https://recrutement.novate-digital.com/api',
    lockInSeconds: 60,
    child: AppMain(),
  );
  iocLocator(configuredApp);
  setupLocator(configuredApp);
  runApp(configuredApp);
}

class AppMain extends StatelessWidget {
  const AppMain({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RootScreen();
  }
}
