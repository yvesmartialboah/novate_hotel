// ignore_for_file: import_of_legacy_library_into_null_safe

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ioc/ioc.dart';
import 'package:novate_hotel_app/components/custom_button.dart';
import 'package:novate_hotel_app/components/custom_dialog.dart';
import 'package:novate_hotel_app/components/inputs/input_field.dart';
import 'package:novate_hotel_app/components/tab_app_bar.dart';
import 'package:novate_hotel_app/constants/colors.dart';
import 'package:novate_hotel_app/models/responses/register-user-response/register_user_response.dart';
import 'package:novate_hotel_app/models/schemas/user/user.dart';
import 'package:novate_hotel_app/screens/dashboard/bottom_tab_navigator.dart';
import 'package:novate_hotel_app/screens/dashboard/home_tab/home_screen.dart';
import 'package:novate_hotel_app/services/user_api.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  RegistrationScreenState createState() => RegistrationScreenState();
}

class RegistrationScreenState extends State<RegistrationScreen> {
  final _formKey = GlobalKey<FormState>();
  final UserApi userApi = Ioc().use('userApi');

  final User _currentUser = User.withValues(
      phone: '', firstname: '', lastname: '', password: '', email: '');

  String _confirmPassword = '';
  bool _registeringLoader = false;

  @override
  void initState() {
    super.initState();
  }

  void onChangedLastname(String text) {
    setState(() {
      _currentUser.lastname = text;
    });
  }

  void onChangedFirstname(String text) {
    setState(() {
      _currentUser.firstname = text;
    });
  }

  void onChangedEmail(String text) {
    setState(() {
      _currentUser.email = text;
    });
  }

  void onChangedPhone(String text) {
    setState(() {
      _currentUser.phone = text;
    });
  }

  void onChangedPassword(String text) {
    setState(() {
      _currentUser.password = text;
    });
  }

  void onChangedConfirmPassword(String text) {
    setState(() {
      _confirmPassword = text;
    });
    print(_confirmPassword);
  }

  void _showDialog(String message) {
    showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return CustomDialog(contextProp: context, messageProp: message);
      },
    );
  }

  void registerUser() async {
    setState(() {
      _registeringLoader = true;
    });

    try {
      RegisterUserResponse registerUserResponse =
          await userApi.registerUser(_currentUser);

      await setUserPreferences(registerUserResponse);
      setState(() {
        _registeringLoader = false;
      });
      await Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => const BottomTabNavigator()),
        (Route<dynamic> route) => false,
      );
    } catch (error) {
      setState(() {
        _registeringLoader = false;
      });
      _showDialog(error.toString());
    }
  }

  Future setUserPreferences(RegisterUserResponse registerUserResponse) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('user', json.encode(registerUserResponse));
    // await prefs.setString('id', registerUserResponse.id);
    await prefs.setString('firstname', registerUserResponse.firstname);
    await prefs.setString('lastname', registerUserResponse.lastname);
    await prefs.setString('email', registerUserResponse.email);
    await prefs.setString('phone', registerUserResponse.phone);
    await prefs.setString(
        'token',
        registerUserResponse
            .password); //I am using the password as a token because the api does not return a token
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: TabAppBar(
          context: context,
          titleProp: 'Inscription'.toUpperCase(),
          showBackButton: true,
          centerTitle: true),
      body: Column(
        children: <Widget>[
          Expanded(
              child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(left: 25, right: 25, top: 15),
                    child: Column(
                      children: <Widget>[
                        Form(
                          key: _formKey,
                          child: Column(
                            children: <Widget>[
                              InputField(
                                  initialValue: '',
                                  label: 'Nom',
                                  labelText: 'Entrez votre nom',
                                  inputType: TextInputType.text,
                                  onChanged: onChangedLastname),
                              InputField(
                                  initialValue: '',
                                  label: 'Prénom',
                                  labelText: 'Entrez votre prénom',
                                  inputType: TextInputType.text,
                                  onChanged: onChangedFirstname),
                              InputField(
                                  initialValue: '',
                                  label: 'Adresse e-mail',
                                  labelText: 'Entrez votre adresse e-mail',
                                  inputType: TextInputType.emailAddress,
                                  onChanged: onChangedEmail),
                              InputField(
                                  initialValue: '',
                                  label: 'Numéro de téléphone',
                                  labelText: 'Entrez votre numéro de téléphone',
                                  inputType: TextInputType.phone,
                                  onChanged: onChangedPhone),
                              InputField(
                                  initialValue: '',
                                  label: 'Mot de passe',
                                  labelText: '************',
                                  inputType: TextInputType.text,
                                  obscureText: true,
                                  onChanged: onChangedPassword),
                              InputField(
                                  initialValue: '',
                                  label: 'Répétez mot de passe',
                                  labelText: '************',
                                  inputType: TextInputType.text,
                                  obscureText: true,
                                  valueToCheck: _currentUser.password,
                                  onChanged: onChangedConfirmPassword),
                            ],
                          ),
                        ),
                      ],
                    )),
                Container(
                  child: CustomButton(
                      loader: _registeringLoader,
                      contextProp: context,
                      onPressedProp: () {
                        if (_formKey.currentState!.validate()) {
                          registerUser();
                        }
                      },
                      textProp: 'Créer'.toUpperCase()),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const Text(
                      'Avez-vous déjà un compte ?',
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 18,
                          fontWeight: FontWeight.w400),
                    ),
                    TextButton(
                        onPressed: () {},
                        child: const Text(
                          'Connexion',
                          style: TextStyle(
                              color: colorPurlpleShade,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ))
                  ],
                )
              ],
            ),
          )),
        ],
      ),
    );
  }
}
