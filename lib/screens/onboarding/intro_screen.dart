import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:novate_hotel_app/constants/img_urls.dart';
import 'package:novate_hotel_app/components/items/main_logo.dart';
import 'package:novate_hotel_app/constants/colors.dart';
import 'package:novate_hotel_app/screens/onboarding/registration_screen.dart';

class IntroScreen extends StatefulWidget {
  const IntroScreen({Key? key}) : super(key: key);

  @override
  IntroScreenState createState() => IntroScreenState();
}

class IntroScreenState extends State<IntroScreen> {
  @override
  void initState() {
    super.initState();
  }

  void _goToRegistrationScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const RegistrationScreen(),
      ),
    );
  }

  static const TextStyle textStyle =
      TextStyle(color: Colors.white, fontSize: 20);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          decoration: const BoxDecoration(
              color: Colors.white,
              image: DecorationImage(
                  image: AssetImage(bootScreenBackground), fit: BoxFit.cover)),
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(top: 35),
                alignment: Alignment.topCenter,
                child: MainLogo(),
              ),
              const Spacer(
                flex: 2,
              ),
              TextButton(
                onPressed: () {
                  _goToRegistrationScreen();
                },
                child: Container(
                  alignment: Alignment.center,
                  height: 55,
                  width: MediaQuery.of(context).size.width - 60,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Text(
                    "S'inscrire".toUpperCase(),
                    style: const TextStyle(
                        color: colorPurlpleShade,
                        fontSize: 20,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ),
              Container(
                  margin: const EdgeInsets.only(bottom: 40),
                  child: TextButton(
                    onPressed: () {
                      print('Ignorer');
                    },
                    child: Text(
                      'Ignorer'.toUpperCase(),
                      style: textStyle,
                    ),
                  )),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const Text(
                    'Avez-vous déjà un compte ?',
                    style: textStyle,
                  ),
                  TextButton(
                      onPressed: () {},
                      child: const Text(
                        'Connexion',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ))
                ],
              ),
              const Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
