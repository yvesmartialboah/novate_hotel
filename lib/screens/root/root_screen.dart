import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:novate_hotel_app/screens/dashboard/bottom_tab_navigator.dart';
// import 'package:novate_hotel_app/app_config.dart';
// import 'package:novate_hotel_app/main.dart';
// import 'package:ioc/ioc.dart';
import 'package:novate_hotel_app/screens/onboarding/intro_screen.dart';
import 'package:novate_hotel_app/screens/dashboard/home_tab/home_screen.dart';
import 'package:novate_hotel_app/services/navigation_service.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
// import 'package:pedantic/pedantic.dart';

import '../../locator.dart';

class RootScreen extends StatefulWidget {
  const RootScreen({Key? key}) : super(key: key);

  @override
  RootScreenState createState() => RootScreenState();
}

class RootScreenState extends State<RootScreen> with WidgetsBindingObserver {
  String? _token = '';

  final NavigationService _navigationService = locator<NavigationService>();

  @override
  void initState() {
    super.initState();

    _readToken();
    _subscribeAppLifeCycle();
  }

  Future _readToken() async {
    final prefs = await SharedPreferences.getInstance();
    final tokenPref = prefs.getString('token');
    setState(() {
      _token = tokenPref;
    });
  }

  void _subscribeAppLifeCycle() {
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void dispose() {
    super.dispose();
    _unsubscribeAppLifeCycle();
  }

  void _unsubscribeAppLifeCycle() {
    WidgetsBinding.instance!.removeObserver(this);
  }
  
  Widget _getHomeScreen() =>
      _token == null ? const IntroScreen() : const BottomTabNavigator();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('fr'),
      ],
      navigatorKey: _navigationService.navigationKey,
      home: _getHomeScreen(),
    );
  }
}
