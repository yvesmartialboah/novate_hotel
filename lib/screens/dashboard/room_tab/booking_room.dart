import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:novate_hotel_app/components/tab_app_bar.dart';
import 'package:novate_hotel_app/constants/colors.dart';
import 'package:novate_hotel_app/constants/img_urls.dart';
import 'package:novate_hotel_app/screens/dashboard/bottom_tab_navigator.dart';
import 'package:novate_hotel_app/screens/dashboard/room_tab/booking_room_end_date.dart';
import 'package:novate_hotel_app/screens/dashboard/room_tab/booking_room_start_date.dart';

class BookingRoom extends StatefulWidget {
  const BookingRoom({Key? key, required this.startDate, required this.endDate})
      : super(key: key);

  final DateTime startDate;
  final DateTime endDate;

  @override
  BookingRoomState createState() => BookingRoomState();
}

enum PaymentTypes { visa, cash }

class BookingRoomState extends State<BookingRoom> {
  PaymentTypes? selectedPayment = PaymentTypes.visa;
  @override
  void initState() {
    super.initState();
  }

  void _bookingRoom() {
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => const BottomTabNavigator()),
      (Route<dynamic> route) => false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: TabAppBar(
          titleProp: 'Réservation'.toUpperCase(),
          context: context,
          centerTitle: true,
          showBackButton: true),
      body: Column(
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.all(15),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.5,
                          height: 100,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Image.asset(
                              roomImg,
                              width: MediaQuery.of(context).size.width * 0.5,
                              height: 100,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        Container(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              margin: EdgeInsets.only(bottom: 20, left: 15),
                              alignment: Alignment.topCenter,
                              child: const Text(
                                'Suite \n royale',
                                style: TextStyle(
                                    fontWeight: FontWeight.w700, fontSize: 22),
                              ),
                            ),
                            Container(
                              child: Row(
                                children: <Widget>[
                                  const Icon(
                                    Icons.star,
                                    color: colorOrange,
                                  ),
                                  Text(
                                    '4.9',
                                    style: TextStyle(
                                        color: Colors.grey[400], fontSize: 20),
                                  )
                                ],
                              ),
                            )
                          ],
                        ))
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(15),
                    alignment: Alignment.topLeft,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        const Text(
                          'Détail de réservation',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 17),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 12, bottom: 12),
                          child: Text(
                            "Date d'arrivée",
                            style: TextStyle(
                                color: Colors.grey[400], fontSize: 16),
                          ),
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                margin: const EdgeInsets.only(left: 13),
                                child: Row(
                                  children: <Widget>[
                                    Image.asset(calendarOutlined),
                                    Container(
                                      margin: const EdgeInsets.only(left: 8),
                                      child: Text(
                                          DateFormat.yMMMMd('fr')
                                              .format(widget.startDate),
                                          style: const TextStyle(fontSize: 16)),
                                    )
                                  ],
                                ),
                              ),
                              TextButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              const BookingRoomStartDate()));
                                },
                                child: const Text(
                                  'Modifier',
                                  style: TextStyle(
                                      color: colorBackgroundBlue,
                                      fontSize: 16,
                                      decoration: TextDecoration.underline),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 25, bottom: 12),
                          child: Text(
                            "Date de départ",
                            style: TextStyle(
                                color: Colors.grey[400], fontSize: 16),
                          ),
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                margin: const EdgeInsets.only(left: 13),
                                child: Row(
                                  children: <Widget>[
                                    Image.asset(calendarOutlined),
                                    Container(
                                      margin: const EdgeInsets.only(left: 8),
                                      child: Text(
                                          DateFormat.yMMMMd('fr')
                                              .format(widget.endDate),
                                          style: const TextStyle(fontSize: 16)),
                                    )
                                  ],
                                ),
                              ),
                              TextButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              BookingRoomEndDate(
                                                startDate: widget.startDate,
                                              )));
                                },
                                child: const Text(
                                  'Modifier',
                                  style: TextStyle(
                                      color: colorBackgroundBlue,
                                      fontSize: 16,
                                      decoration: TextDecoration.underline),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 25, bottom: 12),
                          child: Text(
                            "Invités",
                            style: TextStyle(
                                color: Colors.grey[400], fontSize: 16),
                          ),
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                margin: const EdgeInsets.only(left: 13),
                                child: Row(
                                  children: <Widget>[
                                    Image.asset(calendarOutlined),
                                    Container(
                                      margin: const EdgeInsets.only(left: 8),
                                      child: const Text('2 Adultes',
                                          style: TextStyle(fontSize: 16)),
                                    )
                                  ],
                                ),
                              ),
                              const Text(
                                'Modifier',
                                style: TextStyle(
                                    color: colorBackgroundBlue,
                                    fontSize: 16,
                                    decoration: TextDecoration.underline),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 15),
                          child: Text(
                            'Mode de paiement',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 17),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Container(
                                  child: Image.asset(
                                    visaIcon,
                                    height: 55,
                                    width: 55,
                                  ),
                                ),
                                Radio<PaymentTypes>(
                                  activeColor: colorPurlpleShade,
                                  value: PaymentTypes.visa,
                                  groupValue: selectedPayment,
                                  onChanged: (PaymentTypes? value) {
                                    setState(() {
                                      selectedPayment = value;
                                    });
                                  },
                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    'Cash',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 17),
                                  ),
                                ),
                                Radio<PaymentTypes>(
                                  activeColor: colorPurlpleShade,
                                  value: PaymentTypes.cash,
                                  groupValue: selectedPayment,
                                  onChanged: (PaymentTypes? value) {
                                    setState(() {
                                      selectedPayment = value;
                                    });
                                  },
                                )
                              ],
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Text(
                            'Récapitulatif',
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                            child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Suite royale',
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.bold),
                            ),
                            Column(
                              children: [
                                Row(
                                  children: [
                                    Icon(
                                      Icons.star,
                                      color: colorOrange,
                                    ),
                                    Text(
                                      '4.9',
                                      style: TextStyle(
                                          color: Colors.grey[400],
                                          fontSize: 20),
                                    )
                                  ],
                                ),
                                Container(
                                  child: Text(
                                    '80 000 F',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 17),
                                  ),
                                )
                              ],
                            )
                          ],
                        ))
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            margin: EdgeInsets.only(bottom: 15),
                            child: const Text(
                              '2 personnes (chambre double)',
                              style: TextStyle(fontSize: 16),
                            )),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              margin: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(10)),
                                  border: Border.all(
                                      color: colorPurlpleShade,
                                      width: 1,
                                      style: BorderStyle.solid)),
                              padding: const EdgeInsets.all(15),
                              child:
                                  Image.asset(climIcon, width: 60, height: 60),
                            ),
                            Container(
                              margin: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(10)),
                                  border: Border.all(
                                      color: colorPurlpleShade,
                                      width: 1,
                                      style: BorderStyle.solid)),
                              padding: const EdgeInsets.all(15),
                              child: Image.asset(tvIcon, width: 60, height: 60),
                            ),
                            Container(
                              margin: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(10)),
                                  border: Border.all(
                                      color: colorPurlpleShade,
                                      width: 1,
                                      style: BorderStyle.solid)),
                              padding: const EdgeInsets.all(15),
                              child: Image.asset(bathroomIcon,
                                  width: 60, height: 60),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Description',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15),
                          child: const Text(
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                            style: TextStyle(fontSize: 16),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(bottom: 15),
                          child: const Text(
                            'Calendrier des versements',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            const Text('À régler maintenant',
                                style: TextStyle(fontSize: 17)),
                            Text(
                              0.toStringAsFixed(2) + ' Fcfa',
                              style: const TextStyle(fontSize: 17),
                            ),
                          ],
                        ),
                        Container(
                          margin: const EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              const Text('À régler sur place',
                                  style: TextStyle(fontSize: 17)),
                              Text(
                                115000.toStringAsFixed(2) + ' Fcfa',
                                style: const TextStyle(fontSize: 17),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 25),
                    padding: const EdgeInsets.all(25),
                    decoration: BoxDecoration(color: Colors.yellow.shade200),
                    alignment: Alignment.bottomCenter,
                    child: const Center(
                      child: Text(
                        'Les prix pourrait augmenter si vous réservez plus tard.',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
      bottomNavigationBar: Container(
        height: 81,
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius: 5,
            blurRadius: 5,
          ),
        ]),
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                alignment: Alignment.topLeft,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      'Total                             ',
                      style: TextStyle(color: Colors.grey[400]),
                    ),
                    Text(
                      '80 000 Fcfa',
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                      ),
                    ),
                    Text(
                      'Prix de nuitée              ',
                      style: TextStyle(color: Colors.grey[400]),
                    ),
                  ],
                )),
            Container(
              child: TextButton(
                onPressed: () {
                  _bookingRoom();
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: colorPurlpleShade,
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  alignment: Alignment.center,
                  height: 45,
                  child: Text(
                    'Réserver'.toUpperCase(),
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
