import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:novate_hotel_app/components/tab_app_bar.dart';
import 'package:novate_hotel_app/constants/colors.dart';
import 'package:novate_hotel_app/constants/img_urls.dart';

import 'package:novate_hotel_app/screens/dashboard/bottom_tab_navigator.dart';

class BookingList extends StatefulWidget {
  const BookingList({Key? key}) : super(key: key);

  @override
  BookingListState createState() => BookingListState();
}

class BookingListState extends State<BookingList> {
  @override
  void initState() {
    super.initState();
  }

  void _bookingRoom() {
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => const BottomTabNavigator()),
      (Route<dynamic> route) => false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: TabAppBar(
          titleProp: 'Mes réservations'.toUpperCase(),
          context: context,
          centerTitle: true,
          showBackButton: true),
      body: Column(
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  for (var i = 0; i < 10; i++)
                    Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.all(15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(8),
                                      child: Image.asset(roomImg,
                                          width: 90,
                                          height: 70,
                                          fit: BoxFit.fill),
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(left: 10),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          margin:
                                              const EdgeInsets.only(bottom: 15),
                                          child: const Text(
                                            'Suite royale',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 17),
                                          ),
                                        ),
                                        const Text(
                                          '1 Nuit(s)',
                                          style: TextStyle(fontSize: 17),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              Column(
                                children: <Widget>[
                                  Container(
                                    margin: const EdgeInsets.only(bottom: 15),
                                    child: Row(
                                      children: <Widget>[
                                        const Icon(
                                          Icons.star,
                                          color: colorOrange,
                                        ),
                                        Text(
                                          '4.9',
                                          style: TextStyle(
                                              color: Colors.grey[400],
                                              fontSize: 20),
                                        )
                                      ],
                                    ),
                                  ),
                                  const Text(
                                    '80 000 F',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 17),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                            margin: const EdgeInsets.symmetric(horizontal: 15),
                            child: const Divider(
                              thickness: 1.0,
                              color: Color.fromRGBO(0, 0, 0, 0.12),
                            ))
                      ],
                    ),
                ],
              ),
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: colorDarkBlue,
        onPressed: () {},
        child: Image.asset(
          ticketStarIcon,
          width: 30,
          height: 30,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
