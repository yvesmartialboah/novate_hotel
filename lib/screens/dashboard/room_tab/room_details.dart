import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:novate_hotel_app/components/custom_button.dart';
import 'package:novate_hotel_app/constants/colors.dart';
import 'package:novate_hotel_app/constants/img_urls.dart';
import 'package:novate_hotel_app/screens/dashboard/bottom_tab_navigator.dart';
import 'package:novate_hotel_app/screens/dashboard/home_tab/home_router.dart';
import 'package:intl/intl.dart';
import 'package:novate_hotel_app/screens/dashboard/room_tab/booking_room_end_date.dart';

class RoomDetails extends StatefulWidget {
  const RoomDetails({Key? key}) : super(key: key);

  @override
  RoomDetailsState createState() => RoomDetailsState();
}

class RoomDetailsState extends State<RoomDetails> {
  @override
  void initState() {
    super.initState();
  }

  DateTime selectedDate = DateTime.now();
  final firstDate = DateTime.now().subtract(const Duration(days: 1));
  final lastDate = DateTime(2050, 12);

  void _toggleBottomNavigator(bool show) async {
    final BottomTabNavigatorState? bottomTabNavigator =
        context.findAncestorStateOfType<BottomTabNavigatorState>();
    if (bottomTabNavigator != null && show) {
      bottomTabNavigator.showBottomTabNavigator();
    } else if (bottomTabNavigator != null && !show) {
      bottomTabNavigator.hideBottomTabNavigator();
    }
  }

  void _goBack() {
    _toggleBottomNavigator(true);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => HomeRouter(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      _toggleBottomNavigator(false);
    });
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.5,
              alignment: Alignment.topLeft,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(roomImg), fit: BoxFit.cover)),
              child: Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                    color: colorBorderGreyPrimary.withOpacity(0.7),
                    borderRadius: BorderRadius.all(Radius.circular(50))),
                margin: const EdgeInsets.only(top: 15, left: 15, right: 15),
                alignment: Alignment.center,
                child: TextButton(
                  onPressed: () {
                    _goBack();
                  },
                  child: const Icon(
                    Icons.arrow_back_ios,
                    size: 30,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.4),
              alignment: Alignment.bottomCenter,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.5,
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30),
                      topLeft: Radius.circular(30))),
              child: Column(
                children: <Widget>[
                  Expanded(
                      child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 25, left: 20, right: 20),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                child: Text(
                                  'Suite royale',
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Container(
                                  child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    'Prix de nuitée',
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.grey.shade400),
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Icon(
                                            Icons.star,
                                            color: colorOrange,
                                          ),
                                          Text(
                                            '4.9',
                                            style: TextStyle(
                                                color: Colors.grey[400],
                                                fontSize: 20),
                                          )
                                        ],
                                      ),
                                      Container(
                                        child: Text(
                                          '80 000 F',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17),
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              ))
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 15),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  margin: EdgeInsets.only(bottom: 15),
                                  child: const Text(
                                    '2 personnes (chambre double)',
                                    style: TextStyle(fontSize: 16),
                                  )),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    margin: const EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(10)),
                                        border: Border.all(
                                            color: colorPurlpleShade,
                                            width: 1,
                                            style: BorderStyle.solid)),
                                    padding: const EdgeInsets.all(15),
                                    child: Image.asset(climIcon,
                                        width: 60, height: 60),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(10)),
                                        border: Border.all(
                                            color: colorPurlpleShade,
                                            width: 1,
                                            style: BorderStyle.solid)),
                                    padding: const EdgeInsets.all(15),
                                    child: Image.asset(tvIcon,
                                        width: 60, height: 60),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(10)),
                                        border: Border.all(
                                            color: colorPurlpleShade,
                                            width: 1,
                                            style: BorderStyle.solid)),
                                    padding: const EdgeInsets.all(15),
                                    child: Image.asset(bathroomIcon,
                                        width: 60, height: 60),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(15),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'Description',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 15),
                                child: const Text(
                                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                                  style: TextStyle(fontSize: 16),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ))
                ],
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: Container(
        height: 81,
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius: 5,
            blurRadius: 5,
          ),
        ]),
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.1),
                      spreadRadius: 5,
                      blurRadius: 5,
                    )
                  ],
                  borderRadius: const BorderRadius.all(Radius.circular(7))),
              child: TextButton(
                child: Image.asset(
                  heartEmptyIcon,
                  height: 25,
                  width: 25,
                ),
                onPressed: () {},
              ),
            ),
            Container(
              child: TextButton(
                onPressed: () {
                  // _bookingRoom();
                },
                child: Container(
                  decoration: const BoxDecoration(
                      color: colorPurlpleShade,
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  alignment: Alignment.center,
                  height: 45,
                  child: Text(
                    'Réserver maintenant'.toUpperCase(),
                    style: const TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
