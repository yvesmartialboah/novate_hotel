import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:novate_hotel_app/components/custom_button.dart';
import 'package:novate_hotel_app/constants/colors.dart';
import 'package:novate_hotel_app/constants/img_urls.dart';
import 'package:novate_hotel_app/screens/dashboard/bottom_tab_navigator.dart';
import 'package:novate_hotel_app/screens/dashboard/home_tab/home_router.dart';
import 'package:intl/intl.dart';
import 'package:novate_hotel_app/screens/dashboard/room_tab/booking_room_end_date.dart';

class BookingRoomStartDate extends StatefulWidget {
  const BookingRoomStartDate({Key? key}) : super(key: key);

  @override
  BookingRoomStartDateState createState() => BookingRoomStartDateState();
}

class BookingRoomStartDateState extends State<BookingRoomStartDate> {
  @override
  void initState() {
    super.initState();
  }

  DateTime selectedDate = DateTime.now();
  final firstDate = DateTime.now().subtract(const Duration(days: 1));
  final lastDate = DateTime(2050, 12);

  void _toggleBottomNavigator(bool show) async {
    final BottomTabNavigatorState? bottomTabNavigator =
        context.findAncestorStateOfType<BottomTabNavigatorState>();
    if (bottomTabNavigator != null && show) {
      bottomTabNavigator.showBottomTabNavigator();
    } else if (bottomTabNavigator != null && !show) {
      bottomTabNavigator.hideBottomTabNavigator();
    }
  }

  void _goBack() {
    _toggleBottomNavigator(true);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => HomeRouter(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      _toggleBottomNavigator(false);
    });
    return Scaffold(
      body: SafeArea(
        child: Container(
            decoration: const BoxDecoration(
                color: colorGreyShadeLight,
                image: DecorationImage(
                    image: AssetImage(roomImg), fit: BoxFit.cover)),
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(top: 35),
            child: Column(
              children: <Widget>[
                Expanded(
                    child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height - 60,
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(30),
                                topLeft: Radius.circular(30))),
                        child: Column(
                          children: <Widget>[
                            Container(
                              // alignment: Alignment.topLeft,
                              padding: const EdgeInsets.only(
                                  top: 10, left: 15, right: 15),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    alignment: Alignment.topLeft,
                                    child: TextButton(
                                      onPressed: () {
                                        _goBack();
                                      },
                                      child: const Icon(
                                        Icons.arrow_back_ios,
                                        size: 30,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(top: 15),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: const Text(
                                      'Quand souhaitez-vous arriver chez nous ?',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 25,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    margin: const EdgeInsets.only(top: 20),
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      'Choisissez une date',
                                      style: TextStyle(
                                          color: Colors.grey[400],
                                          fontSize: 17),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              margin: const EdgeInsets.only(top: 20),
                              height: MediaQuery.of(context).size.height -
                                  260, //- 500,
                              padding: const EdgeInsets.all(30),
                              decoration: const BoxDecoration(
                                  color: colorGreyShadeLight,
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(30),
                                      topRight: Radius.circular(30))),
                              child: SingleChildScrollView(
                                child: Column(
                                  children: <Widget>[
                                    CalendarDatePicker(
                                        initialDate: selectedDate,
                                        firstDate: firstDate,
                                        lastDate: lastDate,
                                        onDateChanged: (date) {
                                          setState(() {
                                            selectedDate = date;
                                          });
                                        }),
                                    Container(
                                        child: Row(
                                      children: <Widget>[
                                        Container(
                                          margin:
                                              const EdgeInsets.only(right: 5),
                                          child: Image.asset(
                                            calendarOutlined,
                                            width: 20,
                                            height: 35,
                                          ),
                                        ),
                                        Text(
                                          DateFormat.yMMMMd('fr')
                                              .format(selectedDate),
                                          style: const TextStyle(fontSize: 17),
                                        ),
                                      ],
                                    )),
                                    Container(
                                      margin: const EdgeInsets.only(top: 40),
                                      child: CustomButton(
                                          contextProp: context,
                                          onPressedProp: () {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    BookingRoomEndDate(
                                                  startDate: selectedDate,
                                                ),
                                              ),
                                            );
                                          },
                                          textProp: 'Suivant'.toUpperCase()),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ))
              ],
            )),
      ),
    );
  }
}
