import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:novate_hotel_app/components/custom_button.dart';
import 'package:novate_hotel_app/components/items/logo_purple.dart';
import 'package:novate_hotel_app/components/navigation_drawer.dart';
import 'package:novate_hotel_app/constants/colors.dart';
import 'package:novate_hotel_app/constants/img_urls.dart';
import 'package:novate_hotel_app/screens/dashboard/room_tab/booking_room_start_date.dart';
import 'package:novate_hotel_app/screens/dashboard/room_tab/room_details.dart';
import 'package:novate_hotel_app/screens/dashboard/room_tab/room_router.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    _getUser();
    super.initState();
  }

  String? userFirstname = '';
  String? userLastname = '';

  _refresh() {
    print('Its works');
  }

  void _getUser() async {
    final prefs = await SharedPreferences.getInstance();
    final firstnamePref = prefs.getString('firstname');
    final lastnamePref = prefs.getString('lastname');

    setState(() {
      userFirstname = firstnamePref;
      userLastname = lastnamePref;
    });
  }

  PreferredSize _buildAppBar(BuildContext context) {
    return PreferredSize(
      preferredSize: const Size.fromHeight(100),
      child: SafeArea(
        child: Container(
          decoration: const BoxDecoration(color: Colors.white),
          child: Padding(
            padding: const EdgeInsets.all(13),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                TextButton(
                  onPressed: () {
                    _scaffoldKey.currentState!.openDrawer();
                  },
                  child: SizedBox(
                    height: 30,
                    width: 30,
                    child: Image.asset(
                      menuIcon,
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
                LogoPurple(),
                SizedBox(
                  height: 30,
                  width: 30,
                  child: Image.asset(
                    notificationIcon,
                    fit: BoxFit.contain,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildSlideItem(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(right: 20),
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const RoomDetails(),
                ),
              );
            },
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: Image.asset(
                roomImg,
                fit: BoxFit.contain,
              ),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 10, bottom: 10),
          padding: const EdgeInsets.only(right: 25),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              const Text(
                'Chambre côté Mer',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              Row(
                children: [
                  Icon(
                    Icons.star,
                    color: Colors.yellow.shade900,
                    size: 18,
                  ),
                  Text(
                    '4.9',
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        color: Colors.grey.shade400),
                  )
                ],
              )
            ],
          ),
        ),
        Container(
          alignment: Alignment.topLeft,
          child: RichText(
              text: TextSpan(
                  text: 'À partir de ',
                  style: TextStyle(color: Colors.grey.shade400),
                  children: const <TextSpan>[
                TextSpan(
                    text: '80 000 F/Nuitée',
                    style: TextStyle(color: Colors.black))
              ])),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: NavigationDrawer(
        userFullName: '$userLastname $userLastname',
      ),
      appBar: _buildAppBar(context),
      body: SafeArea(
        child: Container(
          decoration: const BoxDecoration(color: colorGreyShadeLight),
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 30),
          child: Column(
            children: <Widget>[
              Expanded(
                  child: RefreshIndicator(
                onRefresh: () async {
                  _refresh();
                },
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        alignment: Alignment.topLeft,
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              child: const Text(
                                'Profitez de nos',
                                style: TextStyle(
                                    fontWeight: FontWeight.w300, fontSize: 27),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              child: const Text(
                                'Royals Suites',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 27),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 20),
                        alignment: Alignment.topLeft,
                        child: Column(
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(bottom: 10),
                              alignment: Alignment.topLeft,
                              child: Text(
                                'Réservez dès maintenant',
                                style: TextStyle(
                                    color: Colors.grey.shade400, fontSize: 17),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 12, vertical: 12),
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.1),
                                      spreadRadius: 5,
                                      blurRadius: 5,
                                    ),
                                  ],
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5)),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    padding: const EdgeInsets.only(right: 15),
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          margin: const EdgeInsets.symmetric(
                                              horizontal: 5),
                                          child: Image.asset(userOutlined),
                                          width: 20,
                                          height: 35,
                                        ),
                                        const Text(
                                          '02 Adultes',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: colorDarkBlue),
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    height: 35,
                                    decoration: BoxDecoration(
                                        border: Border(
                                            right: BorderSide(
                                                color: Colors.grey.shade400,
                                                width: 1,
                                                style: BorderStyle.solid))),
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        margin: const EdgeInsets.symmetric(
                                            horizontal: 5),
                                        child: Image.asset(calendarOutlined),
                                        width: 20,
                                        height: 35,
                                      ),
                                      const Text(
                                        '11 Oct - 12 Oct',
                                        style: TextStyle(
                                            fontSize: 16, color: colorDarkBlue),
                                      )
                                    ],
                                  ),
                                  Container(
                                    margin: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    child: const Icon(Icons.settings_outlined),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 10),
                              child: CustomButton(
                                  contextProp: context,
                                  onPressedProp: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => RoomRouter(),
                                      ),
                                    );
                                  },
                                  textProp:
                                      'Réserver une chambre'.toUpperCase()),
                            )
                          ],
                        ),
                      ),
                      Container(
                          margin: const EdgeInsets.only(top: 55),
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    'Nos suites'.toUpperCase(),
                                    style: const TextStyle(fontSize: 25),
                                  ),
                                  const Icon(
                                    Icons.search,
                                    size: 32,
                                  )
                                ],
                              ),
                              Container(
                                alignment: Alignment.topLeft,
                                margin: const EdgeInsets.only(
                                  top: 20,
                                ),
                                child: CarouselSlider(
                                  items: [_buildSlideItem(context)],
                                  options: CarouselOptions(
                                    height: 300,
                                    autoPlay: false,
                                    disableCenter: false,
                                    enlargeCenterPage: false,
                                    // viewportFraction: 0.9,
                                    aspectRatio: 2.0,
                                  ),
                                ),
                              )
                            ],
                          )),
                    ],
                  ),
                ),
              ))
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: colorDarkBlue,
        onPressed: () {},
        child: Image.asset(
          ticketStarIcon,
          width: 30,
          height: 30,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
