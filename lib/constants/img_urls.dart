const String bootScreenBackground = 'lib/assets/images/bootscreen-background.png';
const String hotelLogo = 'lib/assets/images/hotel-logo.png';
const String roomImg = 'lib/assets/images/room.png';
const String hotelLogo62 = 'lib/assets/images/novate-hotel-logo62.png';

const String menuIcon = 'lib/assets/icons/menu-icon.png';
const String notificationIcon = 'lib/assets/icons/notification-icon.png';
const String userOutlined = 'lib/assets/icons/user-outlined-icon.png';
const String calendarOutlined = 'lib/assets/icons/calendar-outlined-icon.png';
const String ticketStarIcon = 'lib/assets/icons/ticket-star-icon.png';
const String visaIcon = 'lib/assets/icons/visa-icon.png';

const String bathroomIcon = 'lib/assets/icons/bathroom-icon.png';
const String tvIcon = 'lib/assets/icons/tv-icon.png';
const String climIcon = 'lib/assets/icons/clim-with-bed-icon.png';

const String ticketIcon = 'lib/assets/icons/ticket-icon.png';
const String phoneIcon = 'lib/assets/icons/phone-icon.png';

const String heartEmptyIcon = 'lib/assets/icons/heart-empty.png';
const String heartSelectedIcon = 'lib/assets/icons/heart-selected-icon.png';
