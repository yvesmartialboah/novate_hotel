import 'package:flutter/material.dart';

const Color colorBlue = Color(0xFF41ACEF);
const Color colorPurple = Color(0xFFEB6CD1);
const Color colorGreen = Color(0xFF7ED321);
const Color colorOrange = Color(0xFFF5A623);
const Color colorRed = Color(0xFFD0021B);

const Color colorLightBackgroundBlue = Color(0xFFE6F4FD);
const Color colorBlueSecondary = Color(0xFF6C92EB);
const Color colorBlueTertiary = Color(0xFF80C8F4);
const Color colorBlueQuaternary = Color(0xFFB3DEF9);
const Color colorBlueQuinary = Color(0xFFE6F4FD);
const Color colorBlueSenary = Color(0xFF052438);
const Color colorBackgroundBlue = Color(0xFF094970);
const Color colorCardBlueStart = Color(0xFF5A9CD8);
const Color colorCardBlueEnd = Color(0xFF91CEF4);

const Color colorPurpleSecondary = Color(0xFFB36287);
const Color colorLightBackgroundPurple = Color(0xFFFFEBF4);
const Color colorPurpleTertiary = Color(0xFF791063);
const Color colorDarkPurple = Color(0xFF3C0832);
const Color colorBrightPurple = Color(0xFF791063);
const Color colorPurlpleShade = Color(0xFF4A148C);

const Color colorYellowQuinary = Color(0xFFFAF7F2);

const Color colorBorderGreyPrimary = Color(0xFFDEDEDE);
const Color colorBorderGreySecondary = Color(0xFFE8E8E8);
const Color colorGreyShadeLight = Color(0xFFFAFAFA);

const Color colorDarkGray = Color(0xFF333333);

const Color colorComplementaryPurple = Color(0xFF6C92EB);
const Color colorDarkBlue = Color(0xFF153B61);
const Color colorBorderGreyTertiary = Color(0xFFE0E0E0);
